# Translators:
# Somogyvári Róbert <cr04ch at gmail>, 2009.
# Arpad Biro <biro.arpad gmail>, 2009.
# Balázs Meskó <meskobalazs@mailbox.org>, 2018
# Gyuris Gellért <jobel@ujevangelizacio.hu>, 2018, 2019, 2020, 2023
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2023-07-01 20:31+0000\n"
"Last-Translator: Gyuris Gellért <jobel@ujevangelizacio.hu>, 2023\n"
"Language-Team: Hungarian (https://app.transifex.com/fsf-hu/teams/77907/hu/)\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Somogyvári Róbert <cr04ch at gmail>, 2008\n"
"Gyuris Gellért <jobel at ujevangelizacio.hu>, 2018, 2019, 2020, 2021, 2023"

#. (itstool) path: articleinfo/title
#: tutorial-elements.xml:6
msgid "Elements of design"
msgstr "A tervezés elemei"

#. (itstool) path: articleinfo/subtitle
#: tutorial-elements.xml:7
msgid "Tutorial"
msgstr "Ismertető"

#. (itstool) path: abstract/para
#: tutorial-elements.xml:11
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""
"Az alábbiakban a grafikai tervezés azon elemeit és alapelveit mutatjuk be, "
"melyeket a kezdő művészeti hallgatóknak oktatnak azért, hogy megismertessék "
"velük a művészi rajz különböző sajátságait. Nem egy mindent részletező, "
"teljes listát nyújtunk, Ön is nyugodtan bővítheti vagy javíthatja, hogy még "
"átfogóbbá tegye ezt az ismertetőt."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:25
msgid "Elements of Design"
msgstr "A tervezés elemei"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:26
msgid "The following elements are the building blocks of design."
msgstr "A következő elemek a grafikai tervezés építőkockái:"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:30 elements-f01.svg:132
#, no-wrap
msgid "Line"
msgstr "Vonal"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:31
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""
"A vonal az a hosszal és iránnyal bíró jel, amelyet egy felületen végigmozgó "
"pont hoz létre. A vonalnak változhat a hossza, a szélessége, az iránya, a "
"görbülete és a színe. A vonal lehet sík- (egy tollvonás a papíron) vagy "
"térhatású."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:45 elements-f01.svg:145
#, no-wrap
msgid "Shape"
msgstr "Forma"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:46
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""
"A forma egy határozott alakzat, mely azáltal jön létre, hogy tényleges vagy "
"érzékeltetett vonalak találkozása körülzár egy teret. A szín vagy árnyék "
"változása is meghatározhat egy formát. A formák különféle típusokba "
"sorolhatók: geometriai (négyszög, háromszög, kör) és organikus "
"(szabálytalanságok a körvonalban)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:60 elements-f01.svg:197
#, no-wrap
msgid "Size"
msgstr "Méret"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:61
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""
"A méret az objektumok, vonalak vagy formák nagyságbeli váltakozásaira utal. "
"A méretek különbözősége lehet valóságos vagy látszólagos."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:74 elements-f01.svg:158
#, no-wrap
msgid "Space"
msgstr "Tér"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:75
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""
"A tér egy üres vagy nyitott terület az objektumok között, körül, felett, "
"alatt vagy az objektumok belsejében. Az alakok és formák a körülöttük és "
"bennük levő tér által jönnek létre. A teret gyakran nevezik sík- vagy "
"térhatásúnak. Egy pozitív teret valamilyen alakzat vagy forma tölt ki. Egy "
"negatív teret valamilyen forma vagy alakzat zár körül."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:89 elements-f01.svg:119
#, no-wrap
msgid "Color"
msgstr "Szín"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:90
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""
"A szín a felületről visszaverődő fény hullámhosszának változása által "
"érzékelhető tulajdonság. A szín három mértéke: a SZÍNÁRNYALAT (vagyis a szín "
"neve, mint a piros vagy sárga), az ÉRTÉK (a világosság vagy sötétség) és az "
"INTENZITÁS (a fényesség vagy homályosság)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:104 elements-f01.svg:171
#, no-wrap
msgid "Texture"
msgstr "Textúra"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:105
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""
"A textúra egy felület tapintható (valóságos textúra) vagy látható "
"(látszólagos textúra) anyagi tulajdonsága. A textúra olyan szavakkal írható "
"le, mint érdes, selymes vagy göröngyös."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:118 elements-f01.svg:184
#, no-wrap
msgid "Value"
msgstr "Érték"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:119
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""
"Az értéktől függ, hogy mennyire sötétnek vagy világosnak látszik valami. Az "
"érték változása a színhez adott feketével vagy fehérrel érhető el. A "
"chiaroscuro az értéket használja a fény és az árnyék kompozíción belüli, "
"drámai kontrasztjához."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:133
msgid "Principles of Design"
msgstr "Alapelvek"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:134
msgid "The principles use the elements of design to create a composition."
msgstr ""
"Az elemekből az alapelveket szem előtt tartva alkotható meg a kompozíció."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:138 elements-f01.svg:210
#, no-wrap
msgid "Balance"
msgstr "Egyensúly"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:139
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""
"Az egyensúly az alakok, formák, értékek, színek stb. vizuális "
"egyöntetűségének érzetét jelenti. Az egyensúly lehet szimmetrikus vagy "
"egyenletes, illetve aszimmetrikus vagy nem egyenletes. A kompozíción belüli "
"egyensúly az objektumok, értékek, színek, textúrák, alakok, formák stb. "
"által érhető el."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:153 elements-f01.svg:223
#, no-wrap
msgid "Contrast"
msgstr "Kontraszt"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:154
msgid "Contrast is the juxtaposition of opposing elements"
msgstr ""
"A kontraszt egymástól nagyon különböző vagy éppen egymásnak ellentmondó "
"elemek egymás mellé helyezése által jön létre."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:166 elements-f01.svg:236
#, no-wrap
msgid "Emphasis"
msgstr "Hangsúly"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:167
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""
"A hangsúly a műalkotás bizonyos részeinek kiemelésére és a figyelem "
"megragadására szolgál. Egy rajzon a tekintet először mindig egy érdekes "
"pontra téved."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:180 elements-f01.svg:249
#, no-wrap
msgid "Proportion"
msgstr "Arány"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:181
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""
"Az arány a méret, terület vagy valamilyen dolog mennyiségét egy másikéval "
"összehasonlítva leíró jellemző."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:193 elements-f01.svg:262
#, no-wrap
msgid "Pattern"
msgstr "Minta"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:194
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""
"A minta egy elem (vonal, forma vagy szín) újra és újra ismétlésével hozható "
"létre."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:206 elements-f01.svg:276
#, no-wrap
msgid "Gradation"
msgstr "Átmenet"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:207
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""
"A méret és az irány átmenete vonaltávlatot, míg a szín átmenete melegből "
"hidegbe és a tónus átmenete sötétből világosba légtávlatot eredményez. Az "
"átmenet alkalmas a figyelem felkeltésére, és egy alak mozgását is "
"érzékeltetheti. A sötétből a világosba való átmenet hatására a szem végigfut "
"az alakon."

#. (itstool) path: sect1/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:222 elements-f01.svg:289
#, no-wrap
msgid "Composition"
msgstr "Kompozíció"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:223
msgid "The combining of distinct elements to form a whole."
msgstr ""
"Különböző elemek teljes egésszé szerkesztésével alkotható meg a kompozíció."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:235
msgid "Bibliography"
msgstr "Irodalomjegyzék"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:236
msgid ""
"This is a partial bibliography used to build this document (links may no "
"longer work today or link to malicious sites now, click with care)."
msgstr ""
"Ez egy részleges bibliográfia, amelyből ez a dokumentum készült (a linkek ma "
"már nem működnek, vagy rosszindulatú oldalakra vezetnek, kattintson "
"óvatosan)."

#. (itstool) path: listitem/para
#: tutorial-elements.xml:242
msgid ""
"<ulink url=\"http://www.makart.com/resources/artclass/EPlist.html\">http://"
"www.makart.com/resources/artclass/EPlist.html</ulink>"
msgstr ""
"<ulink url=\"http://www.makart.com/resources/artclass/EPlist.html\">http://"
"www.makart.com/resources/artclass/EPlist.html</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:247
msgid ""
"<ulink url=\"http://www.princetonol.com/groups/iad/Files/elements2."
"htm\">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>"
msgstr ""
"<ulink url=\"http://www.princetonol.com/groups/iad/Files/elements2."
"htm\">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:252
msgid ""
"<ulink url=\"http://www.johnlovett.com/test.htm\">http://www.johnlovett.com/"
"test.htm</ulink>"
msgstr ""
"<ulink url=\"http://www.johnlovett.com/test.htm\">http://www.johnlovett.com/"
"test.htm</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:257
msgid ""
"<ulink url=\"http://digital-web.com/articles/elements_of_design/\">http://"
"digital-web.com/articles/elements_of_design/</ulink>"
msgstr ""
"<ulink url=\"http://digital-web.com/articles/elements_of_design/\">http://"
"digital-web.com/articles/elements_of_design/</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:262
msgid ""
"<ulink url=\"http://digital-web.com/articles/principles_of_design/\">http://"
"digital-web.com/articles/principles_of_design/</ulink>"
msgstr ""
"<ulink url=\"http://digital-web.com/articles/principles_of_design/\">http://"
"digital-web.com/articles/principles_of_design/</ulink>"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:267
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink "
"url=\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this "
"tutorial. Also, thanks to the Open Clip Art Library (<ulink url=\"https://"
"openclipart.org/\">https://openclipart.org/</ulink>) and the graphics people "
"have submitted to that project."
msgstr ""
"Külön köszönet Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite</ulink>/) segítségéért, melyet az "
"ismertető elkészítésében nyújtott nekem (<ulink url=\"http://www.rejon.org/"
"\">http://www.rejon.org/</ulink>). Szintén köszönetet az Open Clip Art "
"Librarynek (<ulink url=\"https://openclipart.org/\">http://openclipart.org/</"
"ulink>) és a munkáikkal őket támogató grafikusoknak."

#. (itstool) path: Work/format
#: elements-f01.svg:51 elements-f02.svg:48 elements-f03.svg:48
#: elements-f04.svg:48 elements-f05.svg:48 elements-f06.svg:91
#: elements-f07.svg:91 elements-f08.svg:206 elements-f09.svg:48
#: elements-f10.svg:48 elements-f11.svg:48 elements-f12.svg:445
#: elements-f13.svg:102 elements-f14.svg:173 elements-f15.svg:452
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: elements-f01.svg:92
#, no-wrap
msgid "Elements"
msgstr "Elemek"

#. (itstool) path: text/tspan
#: elements-f01.svg:106
#, no-wrap
msgid "Principles"
msgstr "Alapelvek"

#. (itstool) path: text/tspan
#: elements-f01.svg:302
#, no-wrap
msgid "Overview"
msgstr "Áttekintés"

#. (itstool) path: text/tspan
#: elements-f04.svg:79
#, no-wrap
msgid "BIG"
msgstr "NAGY"

#. (itstool) path: text/tspan
#: elements-f04.svg:93
#, no-wrap
msgid "small"
msgstr "kicsi"

#. (itstool) path: text/tspan
#: elements-f12.svg:710
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr "Random Ant &amp; 4WD"

#. (itstool) path: text/tspan
#: elements-f12.svg:721
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr "Andrew Fitzsimon SVG-rajza"

#. (itstool) path: text/tspan
#: elements-f12.svg:726
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr "Az Open Clip Art Library szívességéből"

#. (itstool) path: text/tspan
#: elements-f12.svg:731
#, no-wrap
msgid "https://openclipart.org/"
msgstr "https://openclipart.org/"
