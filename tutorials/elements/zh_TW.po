# Dongjun Wu <ziyawu@gmail.com>, 2012, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2021-05-08 22:41+0800\n"
"Last-Translator: Dongjun Wu <ziyawu@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 2.4.1\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "dongjun wu <ziyawu@gmail.com>, 2016,2021"

#. (itstool) path: articleinfo/title
#: tutorial-elements.xml:6
msgid "Elements of design"
msgstr "設計要素"

#. (itstool) path: articleinfo/subtitle
#: tutorial-elements.xml:7
msgid "Tutorial"
msgstr "指導手冊"

#. (itstool) path: abstract/para
#: tutorial-elements.xml:11
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""
"這篇教學示範設計的要素和原則，在初期教導正規美術學生這些是為了理解用在藝術創"
"作的各種性質。這不是一個詳盡的列表，所以請補充、刪減和結合其他內容讓這個教學"
"更全面。"

#. (itstool) path: sect1/title
#: tutorial-elements.xml:25
msgid "Elements of Design"
msgstr "設計要素"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:26
msgid "The following elements are the building blocks of design."
msgstr "下列幾項元素是設計的積木。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:30 elements-f01.svg:132
#, no-wrap
msgid "Line"
msgstr "線條"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:31
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""
"線條被定義為表示長度和方向的特徵，由一點建立並移動跨越一個表面。線條能有各種"
"長度、寬度、方向、曲率和顏色。線條可以是二維 (紙張上的鉛筆線)，或是隱含的三"
"維。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:45 elements-f01.svg:145
#, no-wrap
msgid "Shape"
msgstr "形狀"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:46
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""
"當實際或隱含的線條接合圍繞一個空間時就建立出一個平面圖形、形狀。顏色或明暗變"
"化可定義一個形狀。形狀可分為幾種類型：幾何 (正方形、三角形、圓形) 和 有機 (不"
"規則的輪廓)。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:60 elements-f01.svg:197
#, no-wrap
msgid "Size"
msgstr "大小"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:61
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""
"這項涉及物件、線條或形狀的比例差異。物件不論在實際或視覺上都有大小差異。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:74 elements-f01.svg:158
#, no-wrap
msgid "Space"
msgstr "空間"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:75
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""
"空間是物件周圍、上面、下面或裡面的空白或開放面積。由空間的周圍和內部製造出形"
"狀和結構。空間常被稱為三維或二維。實空間是由一個形狀或結構填充。虛空間則圍繞"
"一個形狀或結構。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:89 elements-f01.svg:119
#, no-wrap
msgid "Color"
msgstr "顏色"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:90
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""
"顏色是取決於表面反射光波長的感知特性。顏色有三個維度：色相 (顏色的另一個詞，"
"表明了它的名字如紅色或黃色)，亮度 (淺淡或深暗)，飽和度 (鮮豔或黯淡)。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:104 elements-f01.svg:171
#, no-wrap
msgid "Texture"
msgstr "紋理"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:105
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""
"紋理就是表面感覺 (實際紋路)或表現的外觀 (暗示紋理)。紋理的描述詞有粗糙、柔軟"
"光潔或礫質砂等。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:118 elements-f01.svg:184
#, no-wrap
msgid "Value"
msgstr "明暗"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:119
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""
"明暗是物體外觀陰暗或明亮的程度。我們由加入黑色或白色以實現顏色明暗度的變化。"
"明暗對照法經由物體中強烈地光暗對比發揮繪畫中的價值。"

#. (itstool) path: sect1/title
#: tutorial-elements.xml:133
msgid "Principles of Design"
msgstr "設計原則"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:134
msgid "The principles use the elements of design to create a composition."
msgstr "原則是運用設計要素創作出一個組合體。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:138 elements-f01.svg:210
#, no-wrap
msgid "Balance"
msgstr "平衡"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:139
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""
"平衡是一種在形狀、結構、明暗、顏色等上面的視覺均等感受。平衡感可以是對稱的或"
"均勻的平衡或者非對稱的和非均勻的平衡。物件、明暗、顏色、紋理、形狀、結構等可"
"以用於建立一個合成體的平衡感。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:153 elements-f01.svg:223
#, no-wrap
msgid "Contrast"
msgstr "對比"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:154
msgid "Contrast is the juxtaposition of opposing elements"
msgstr "對比是幾項相對的元素並列一起"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:166 elements-f01.svg:236
#, no-wrap
msgid "Emphasis"
msgstr "強調"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:167
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""
"強調是用來使作品的某些部份脫穎而出並引起你的注意。趣味中心或焦點的作用是吸引"
"你的第一次目光。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:180 elements-f01.svg:249
#, no-wrap
msgid "Proportion"
msgstr "比例"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:181
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr "比例是描述一個物體在大小、位置或數量上與另一個物體的差別。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:193 elements-f01.svg:262
#, no-wrap
msgid "Pattern"
msgstr "圖案"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:194
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr "圖案由一項要素 (線條、形狀或顏色) 多次重複所建立而成。"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:206 elements-f01.svg:276
#, no-wrap
msgid "Gradation"
msgstr "層次"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:207
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""
"大小和方向的層次可產生直線透視。從暖色到冷色及暗色到淺色的層次則會產生大氣透"
"視。層次能將趣味性和移動感融入到形狀。一個從暗色到淺色的層次會促使視線沿著形"
"狀移動。"

#. (itstool) path: sect1/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:222 elements-f01.svg:289
#, no-wrap
msgid "Composition"
msgstr "組合"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:223
msgid "The combining of distinct elements to form a whole."
msgstr "不同的要素結合形成一個整體。"

#. (itstool) path: sect1/title
#: tutorial-elements.xml:235
msgid "Bibliography"
msgstr "參考文獻"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:236
msgid ""
"This is a partial bibliography used to build this document (links may no "
"longer work today or link to malicious sites now, click with care)."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:242
msgid ""
"<ulink url=\"http://www.makart.com/resources/artclass/EPlist.html\">http://"
"www.makart.com/resources/artclass/EPlist.html</ulink>"
msgstr ""
"<ulink url=\"http://www.makart.com/resources/artclass/EPlist.html\">http://"
"www.makart.com/resources/artclass/EPlist.html</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:247
msgid ""
"<ulink url=\"http://www.princetonol.com/groups/iad/Files/elements2."
"htm\">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>"
msgstr ""
"<ulink url=\"http://www.princetonol.com/groups/iad/Files/elements2."
"htm\">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:252
msgid ""
"<ulink url=\"http://www.johnlovett.com/test.htm\">http://www.johnlovett.com/"
"test.htm</ulink>"
msgstr ""
"<ulink url=\"http://www.johnlovett.com/test.htm\">http://www.johnlovett.com/"
"test.htm</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:257
msgid ""
"<ulink url=\"http://digital-web.com/articles/elements_of_design/\">http://"
"digital-web.com/articles/elements_of_design/</ulink>"
msgstr ""
"<ulink url=\"http://digital-web.com/articles/elements_of_design/\">http://"
"digital-web.com/articles/elements_of_design/</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:262
msgid ""
"<ulink url=\"http://digital-web.com/articles/principles_of_design/\">http://"
"digital-web.com/articles/principles_of_design/</ulink>"
msgstr ""
"<ulink url=\"http://digital-web.com/articles/principles_of_design/\">http://"
"digital-web.com/articles/principles_of_design/</ulink>"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:267
#, fuzzy
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink "
"url=\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this "
"tutorial. Also, thanks to the Open Clip Art Library (<ulink url=\"https://"
"openclipart.org/\">https://openclipart.org/</ulink>) and the graphics people "
"have submitted to that project."
msgstr ""
"特別感謝 Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/\">http://"
"www.redlucite.org/redlucite/</ulink>) 給予我 (<ulink url=\"http://www.rejon."
"org/\">http://www.rejon.org/</ulink>) 和這篇教學的幫助。也感謝開放美工圖庫 "
"(<ulink url=\"http://www.openclipart.org/\">http://www.openclipart.org/</"
"ulink>) 和遞交到這個計劃的作品、作者。"

#. (itstool) path: Work/format
#: elements-f01.svg:51 elements-f02.svg:48 elements-f03.svg:48
#: elements-f04.svg:48 elements-f05.svg:48 elements-f06.svg:91
#: elements-f07.svg:91 elements-f08.svg:206 elements-f09.svg:48
#: elements-f10.svg:48 elements-f11.svg:48 elements-f12.svg:445
#: elements-f13.svg:102 elements-f14.svg:173 elements-f15.svg:452
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: elements-f01.svg:92
#, no-wrap
msgid "Elements"
msgstr "要素"

#. (itstool) path: text/tspan
#: elements-f01.svg:106
#, no-wrap
msgid "Principles"
msgstr "原則"

#. (itstool) path: text/tspan
#: elements-f01.svg:302
#, no-wrap
msgid "Overview"
msgstr "概要"

#. (itstool) path: text/tspan
#: elements-f04.svg:79
#, no-wrap
msgid "BIG"
msgstr "BIG"

#. (itstool) path: text/tspan
#: elements-f04.svg:93
#, no-wrap
msgid "small"
msgstr "small"

#. (itstool) path: text/tspan
#: elements-f12.svg:710
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr "Random Ant &amp; 4WD"

#. (itstool) path: text/tspan
#: elements-f12.svg:721
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr "SVG 圖像作者為 Andrew Fitzsimon"

#. (itstool) path: text/tspan
#: elements-f12.svg:726
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr "開放美工圖庫的美意"

#. (itstool) path: text/tspan
#: elements-f12.svg:731
#, fuzzy, no-wrap
msgid "https://openclipart.org/"
msgstr "http://www.openclipart.org/"

#~ msgid "This is a partial bibliography used to build this document."
#~ msgstr "這是用於文件的部份參考文獻。"

#~ msgid "http://www.makart.com/resources/artclass/EPlist.html"
#~ msgstr "http://www.makart.com/resources/artclass/EPlist.html"

#~ msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
#~ msgstr "http://www.princetonol.com/groups/iad/Files/elements2.htm"

#~ msgid "http://www.johnlovett.com/test.htm"
#~ msgstr "http://www.johnlovett.com/test.htm"

#~ msgid "http://digital-web.com/articles/elements_of_design/"
#~ msgstr "http://digital-web.com/articles/elements_of_design/"

#~ msgid "http://digital-web.com/articles/principles_of_design/"
#~ msgstr "http://digital-web.com/articles/principles_of_design/"

#~ msgid "http://sanford-artedventures.com/study/study.html"
#~ msgstr "http://sanford-artedventures.com/study/study.html"

#~ msgid "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
#~ msgstr "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
