# Inkscape keyboard and mouse reference

This is where keys are kept in [DocBook 4.5](https://tdg.docbook.org/tdg/4.5/docbook.html) format.

## Generating output

Use `make` to generate HTML files from `keys.xml`.

- `make all`          : Create HTML files
- `make html`         : Create HTML files
- `make authors`      : Update `keys-authors.xml` from git log
- `make check-input`  : Validate (non-compiled) input files
- `make check-output` : Validate (compiled) output files
- `make pot`          : Update `keys.pot`
- `make po`           : Update` keys.pot` and all `.po` files
- `make copy`         : Copy generated HTML key files to the export directory
- `make clean`        : Remove all generated HTML key files
- `make version`      : Print installed versions

For help and additional usage information use `make help`.


## Requirements

- `xsltproc` (to transform `keys-html.xsl` and `keys.xml` to `keys.html`)
    example command line for manual creation:
    `xsltproc keys-html.xsl keys.xml > keys.html`
- `itstool` (to manage `.po` files and create localized `keys.xml` from them)
